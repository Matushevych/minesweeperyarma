$(document).ready(function() {
    let classes = ["mine", "number", "number", "number"];
    let alreadyPlayed = true;
    let cell = $(".cell");
    let number = 0;
    let points = 0;
    let numOfMines = 0;
    let numOfFlags = 0;
    let numOfFlagsAround = 0;
    let index = 0;


////////////////CREATING MINES AND POINTS IN RANDOM CELLS
    $(cell).each(function() {
        numOfMines = $(".mine").length;
        if (numOfMines >= 10) {
            if (alreadyPlayed) {
                classes.splice(0, 1);
                alreadyPlayed = false;
            }
        }
        let randomClass = classes[~~(Math.random() * classes.length)];
        $(this).append(`<div class = ${randomClass}></div>`);

        $(".flagsMines").text(`flags/mines: ${numOfFlags}/${numOfMines}`);
    });

////////////////RIGHT CLICK, CREATING FLAGS
    $(cell).contextmenu(function() {
        if ($(this).children("div").is(":hidden")) {
            $(this).toggleClass("flag");
        }
        numOfFlags = $(".flag").length;
        $(".flagsMines").text(`flags/mines: ${numOfFlags}/${numOfMines}`);
    });

////////////////SOME CONDITIONS
    for (let l = 0; l <= 56; l += 8) {
        if (!$(cell[l]).hasClass("mine")) {
            $(cell[l]).addClass("left");
        }
    }

    for (let r = 7; r <= 64; r += 8) {
        if (!$(cell[r]).hasClass("mine")) {
            $(cell[r]).addClass("right");
        }
    }

////////////////NUMBER OF MINES NEAR THE ACTUAL CELL
    for (let k = 0; k <= $(cell).length - 1; k++) {
        if (!$(cell[k]).children("div").hasClass("mine")) {
            number = 0;
            $(cell[k]).children("div").text(number);
            if (k > 0 && $(cell[k - 1]).children("div").hasClass("mine") && !$(cell[k]).hasClass("left")) {
                number++;
                $(cell[k]).children("div").text(number);
            }
            if (k < 63 && $(cell[k + 1]).children("div").hasClass("mine") && !$(cell[k]).hasClass("right")) {
                number++;
                $(cell[k]).children("div").text(number);
            }
            if (k < 64 && k > 0 && k < 56 && $(cell[k + 7]).children("div").hasClass("mine") && !$(cell[k]).hasClass("left")) {
                number++;
                $(cell[k]).children("div").text(number);
            }
            if (k < 64 && k < 55 && $(cell[k + 8]).children("div").hasClass("mine")) {
                number++;
                $(cell[k]).children("div").text(number);
            }
            if (k < 64 && k < 55 && $(cell[k + 9]).children("div").hasClass("mine") && !$(cell[k]).hasClass("right")) {
                number++;
                $(cell[k]).children("div").text(number);
            }
            if (k > 0 && k > 7 && $(cell[k - 7]).children("div").hasClass("mine") && !$(cell[k]).hasClass("right")) {
                number++;
                $(cell[k]).children("div").text(number);
            }
            if (k > 0 && k > 8 && $(cell[k - 8]).children("div").hasClass("mine")) {
                number++;
                $(cell[k]).children("div").text(number);
            }
            if (k > 0 && k > 8 && $(cell[k - 9]).children("div").hasClass("mine") && !$(cell[k]).hasClass("left")) {
                number++;
                $(cell[k]).children("div").text(number);
            }
        }
    }

////////////////MAIN FUNCTIONALITY AND CONDITIONS OF GAME
    $(cell).click(function() {
        if ($(this).hasClass("flag")) {
            return false;
        }
        $(".restart-btn").css("display", "block");
        $(this).children("div").css("display", "inline-block");
        if ($(this).children("div").hasClass("mine")) {
            $(cell).find(".mine").css("display", "inline-block");
            $(".game-over").css("display", "block");
        }
        index = $(this).index();

        if ($(this).children("div").text() === "0") {
            if (index > 0 && !$(this).hasClass("left") && !$(cell[index - 1]).hasClass("flag")) {
                $(cell[index - 1]).children("div").css("display", "inline-block");
            }
            if (index < 63 && !$(this).hasClass("right") && !$(cell[index + 1]).hasClass("flag")) {
                $(cell[index + 1]).children("div").css("display", "inline-block");
            }
            if (index < 64 && index > 0 && index < 56 && !$(this).hasClass("left") && !$(cell[index + 7]).hasClass("flag")) {
                $(cell[index + 7]).children("div").css("display", "inline-block");
            }
            if (index < 64 && index < 55 && !$(cell[index + 8]).hasClass("flag")) {
                $(cell[index + 8]).children("div").css("display", "inline-block");
            }
            if (index < 64 && index < 55 && !$(this).hasClass("right") && !$(cell[index + 9]).hasClass("flag")) {
                $(cell[index + 9]).children("div").css("display", "inline-block");
            }
            if (index > 0 && index > 7 && !$(this).hasClass("right") && !$(cell[index - 7]).hasClass("flag")) {
                $(cell[index - 7]).children("div").css("display", "inline-block");
            }
            if (index > 0 && index > 8 && !$(cell[index - 8]).hasClass("flag")) {
                $(cell[index - 8]).children("div").css("display", "inline-block");
            }
            if (index > 0 && index > 8 && !$(this).hasClass("left") && !$(cell[index - 9]).hasClass("flag")) {
                $(cell[index - 9]).children("div").css("display", "inline-block");
            }
        }

        points = 0;
        $(".number").each(pointsCount);
    });

////////////////COUNT POINTS
    function pointsCount() {
        if ($(this).is(":visible")) {
            points += $(this).text().toNum();
            $(".points").text(`points: ${points}`);
        }
    };

////////////////STRING TO NUMBER
    String.prototype.toNum = function() {
        return parseInt(this, 10);
    };

////////////////NUMBER OF MINES NEAR THE ACTUAL CELL
    $(cell).dblclick(function() {
        for (let f = 0; f <= $(this).index(); f++) {
            numOfFlagsAround = 0;
            if (f > 0 && $(cell[f - 1]).hasClass("flag") && !$(cell[f]).hasClass("left")) {
                numOfFlagsAround++;
            }
            if (f < 63 && $(cell[f + 1]).hasClass("flag") && !$(cell[f]).hasClass("right")) {
                numOfFlagsAround++;
            }
            if (f < 64 && f > 0 && f < 56 && $(cell[f + 7]).hasClass("flag") && !$(cell[f]).hasClass("left")) {
                numOfFlagsAround++;
            }
            if (f < 64 && f < 55 && $(cell[f + 8]).hasClass("flag")) {
                numOfFlagsAround++;
            }
            if (f < 64 && f < 55 && $(cell[f + 9]).hasClass("flag") && !$(cell[f]).hasClass("right")) {
                numOfFlagsAround++;
            }
            if (f > 0 && f > 7 && $(cell[f - 7]).hasClass("flag") && !$(cell[f]).hasClass("right")) {
                numOfFlagsAround++;
            }
            if (f > 0 && f > 8 && $(cell[f - 8]).hasClass("flag")) {
                numOfFlagsAround++;
            }
            if (f > 0 && f > 8 && $(cell[f - 9]).hasClass("flag") && !$(cell[f]).hasClass("left")) {
                numOfFlagsAround++;
            }
        }
        $(".flagsMines").text(`flags/mines: ${numOfFlags}/${numOfMines}`);
////////////////IF FLAGS = ACTUAL DIGIT, OPEN CELLS NEARBY
        index = $(this).index();

        if (numOfFlags > 0 && $(this).children("div").text() == numOfFlagsAround) {
            if (index > 0 && !$(this).hasClass("left")) {
                $(cell[index - 1]).children("div").css("display", "inline-block");
                $(cell[index - 1]).removeClass("flag");
            }
            if (index < 63 && !$(this).hasClass("right")) {
                $(cell[index + 1]).children("div").css("display", "inline-block");
                $(cell[index + 1]).removeClass("flag");
            }
            if (index < 64 && index > 0 && index < 56 && !$(this).hasClass("left")) {
                $(cell[index + 7]).children("div").css("display", "inline-block");
                $(cell[index + 7]).removeClass("flag");
            }
            if (index < 64 && index < 55) {
                $(cell[index + 8]).children("div").css("display", "inline-block");
                $(cell[index + 8]).removeClass("flag");
            }
            if (index < 64 && index < 55 && !$(this).hasClass("right")) {
                $(cell[index + 9]).children("div").css("display", "inline-block");
                $(cell[index + 9]).removeClass("flag");
            }
            if (index > 0 && index > 7 && !$(this).hasClass("right")) {
                $(cell[index - 7]).children("div").css("display", "inline-block");
                $(cell[index - 7]).removeClass("flag");
            }
            if (index > 0 && index > 8) {
                $(cell[index - 8]).children("div").css("display", "inline-block");
                $(cell[index - 8]).removeClass("flag");
            }
            if (index > 0 && index > 8 && !$(this).hasClass("left")) {
                $(cell[index - 9]).children("div").css("display", "inline-block");
                $(cell[index - 9]).removeClass("flag");
            }
            $(cell).find(".mine:visible").html('<img class="christ" src="img/christ.png">');
        }
    });

////////////////RESTART GAME
    $(".restart-btn").click(function() {
        document.location.reload(true);
    });

////////////////SUCCESSFUL GAME
    if ($(".christ").length == 10) {
        $(".game-over").css("display", "block");
        $(".restart-btn").text("Success!");
        document.location.reload(true);
    }


});
